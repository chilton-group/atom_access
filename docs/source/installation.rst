.. _Installation:

Installation
============

The ``atom_access`` python package and its command line interface can be installed using the ``pip`` package manager

.. code-block:: bash

    pip install atom_access


To test your installation was successful, run the following command

.. code-block:: bash

    atom_access -h

You should see the help text for ``atom_access``.

You are now ready to start using ``atom_access``, head to the :ref:`guide` pages for more information.

Updating
--------

To update ``atom_access``, run

.. code-block:: bash

    pip install atom_access --upgrade

Terminal output
---------------

Mac and Linux users will see a colour-coded terminal output. By default this is disabled for Windows users since ``CMD`` does
not support ASCII colour codes. If you are on Windows and are using an ASCII enabled terminal (e.g. Windows Terminal) you can
enable colour-coded output by setting the ``aa_term_color`` environment variable to any value in the system environment variable window.

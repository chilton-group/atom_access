Contributors
============

`Prof. Nicholas Chilton <https://www.nfchilton.com>`_

`Dr Gemma Gransbury <https://www.nfchilton.com/people>`_

`Dr Jon Kragskow <https://www.kragskow.dev>`_

We request that any results obtained through the use of `atom_access` are accompanied by the following reference:

Gransbury, G. K.; Corner, S. C.; Kragskow, J. G. C.; Evans, P.; Yeung, H. M.; Blackmore, W. J. A.; Whitehead, G. F. S.; Oakley, M. S.; Vitorica-Yrezabal, I. J.; Chilton, N. F.; Mills, D. P. *AtomAccess*: A predictive tool for molecular design and its application to the targeted synthesis of dysprosium single-molecule magnets. *Journal of the American Chemical Society* **2023**, DOI: 10.1021/jacs.3c08841.

This code was developed under the ERC CoG-816268 grant with PI `David P. Mills <http://millsgroup.weebly.com/>`_. 

We acknowledge Ken Gransbury for help conceptualising the ``atom_access`` logo
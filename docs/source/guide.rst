.. _guide:

=====
Guide
=====

AtomAccess can be run in the command line as follows

.. code-block:: bash

    atom_access <molecule.xyz>

where ``<molecule.xyz>`` is the name of the xyz file with atomic coordinates.
The xyz file is in standard format, with the first line as the number of atoms,
the second line as a comment line and the subsequent lines giving atomic symbols
and x, y, z atomic coordinates in Angstroms. The atomic symbols are accepted
with or without indicies, i.e. C or C1.

Additional arguments given can be added to the command to change the default
settings and turn on other options. Multiple additional arguments can be 
combined. The default values and additional arguments are given below.

**The atom of interest**

Select the atom for which to calculate the accessibility using

.. code-block:: bash

    --atom <integer>
    
or

.. code-block:: bash
    
    -a <integer>

where ``<integer>`` is the index of the desired atom. The first atom listed 
is atom 1, and is the default.

**The radial cutoff**, r\ :sub:`max`

The radial cutoff is the distance from the atom of interest at which the
accessibility is evaluated. Intersections of rays with atoms beyond
r\ :sub:`max` from the atom of interest will be ignored and the ray will be
considered to be unblocked. The radial cutoff can be changed with the
following argument

.. code-block:: bash

    --cutoff <number>
            
or

.. code-block:: bash
    
    -c <number>

where ``<number>`` is r\ :sub:`max` in Angstroms.  The default radial cutoff
value is 5 Angstroms.


**The number of rays**

The number of rays emanating from the atom of interest is controlled by the 
Zaremba-Conroy-Wolfsberg (ZCW) density index (Table 1), calculated using the
ZCW algorithm.\ [#f1]_ The density is changed with the following argument

.. code-block:: bash

    --density <integer>
    
or

.. code-block:: bash
    
    -d <integer>

where ``<integer>`` is an integer between 0-15. The default value is 10, as
this provides a good balance between precise values and accurate
clustering.\ [#f2]_

Table 1: Number of rays for various ZCW densities

+-------------------+----------------+
| ZCW density index | Number of rays |
+===================+================+
| 0                 | 21             |
+-------------------+----------------+ 
| 1                 | 34             |
+-------------------+----------------+ 
| 2                 | 55             |
+-------------------+----------------+ 
| 3                 | 89             |
+-------------------+----------------+ 
| 4                 | 144            |
+-------------------+----------------+ 
| 5                 | 233            |
+-------------------+----------------+ 
| 6                 | 377            |
+-------------------+----------------+ 
| 7                 | 610            |
+-------------------+----------------+ 
| 8                 | 987            |
+-------------------+----------------+ 
| 9                 | 1597           |
+-------------------+----------------+ 
| 10                | 2584           |
+-------------------+----------------+ 
| 11                | 4181           |
+-------------------+----------------+ 
| 12                | 6765           |
+-------------------+----------------+ 
| 13                | 10946          |
+-------------------+----------------+ 
| 14                | 17711          |
+-------------------+----------------+ 
| 15                | 28657          |
+-------------------+----------------+ 

**Quiet mode**

Choose to suppress printing to the terminal and suppress the output header
with the following command

.. code-block:: bash

    --quiet
    
or

.. code-block:: bash

    -q

**Save the unblocked ray objects**

The unblocked ray objects can be saved using `pickle <https://docs.python.org/3/library/pickle.html>`_
with the following command

.. code-block:: bash

    --save_rays
    
or

.. code-block:: bash
    
    -sr


**Plot the unblocked rays**

The preferred way to plot *AtomAccess* results is using the `web interface <https://magnetism-tools.manchester.ac.uk/apps/atom_access_app>`_.
The command line program also allows the unblocked rays to be plotted in a
web browser using `plotly <https://plotly.com/python/>`_. Use the following
command

.. code-block:: bash

    --plot
    
or

.. code-block:: bash
    
    -p


.. rubric:: References
.. [#f1] Eden, M.; Levitt, M. H. *J. Magn. Res.*, **1998**, *132*, 220-239.
.. [#f2] Gransbury, G. K.; Corner, S. C.; Kragskow, J. G. C., Evans, P.; Yeung, H. M.; Blackmore, W. J. A.; Whitehead, G. F. S.; Vitorica-Yrezabal, I. J.; Chilton, N. F.; Mills, D. P. *AtomAccess*: A predictive tool for molecular design and its application to the targeted synthesis of dysprosium single-molecule magnets. *ChemRxiv* **2023**, DOI: 10.26434/chemrxiv-2023-28z84.

*AtomAccess* Manual
===================

.. toctree::
   Installation <installation>
   Guide <guide>
   Modules <modules/index>
   Theory <theory/index>
   Contributors <contribs>
   Web Interface <https://atom-access.com>
   Source Code <https://gitlab.com/chilton-group/atom_access>
   License <https://gitlab.com/chilton-group/atom_access/-/blob/main/LICENSE>
   Paper <https://pubs.acs.org/doi/10.1021/jacs.3c08841>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

This is the documentation for ``atom_access``, a python package developed by the `Chilton Group <https://www.nfchilton.com>`_ which uses ray tracing
to calculate steric hindrance. 


New users
^^^^^^^^^

First visit :ref:`installation` to see how to set up ``atom_access``, then check out the :ref:`guide` pages for instructions on how to use ``atom_access``.

.. _theory:

Theory
======


.. toctree::
   :maxdepth: 2

    Defining Spheres and Rays <rays>
    Ray Tracing <trace>
    Ray Clustering <cluster>
.. _rays:

Defining Spheres and Rays
=========================

The first step in ray tracing steric hindrance is to define the molecule as a set of spheres
with given radii, and to define the rays as a number of vectors emanating (evenly) from a given location.

To define the spheres with which the rays interact, ``atom_access`` first shifts the
cartesian coordinates of the molecule such that the atom interest is centred at the origin.
Then, each atom is assigned a spherical radius from a list of known van der Waals atomic radii.\ [#f1]_
Finally, we discard all atoms which have no intersection with a spherical region of radius :math:`r_\text{cutoff}`
centered on the central atom from the subsequent :ref:`ray tracing <trace>` process.

An angular Zaremba-Conroy-Wolfsberg (ZCW) grid with variable density index :math:`\rho`,\ [#f2]_ is used
to define a set of rays emanating from the origin (atom of interest) which evenly sample the surface of a sphere.
    
.. rubric:: References
.. [#f1] CRC Handbook of Chemistry and Physics, 97th Ed.; W. H. Haynes Ed. CRC Press/Taylor and Francis: Boca Raton, 2016 (accessed 2020-10-01).
.. [#f2] Eden, M.; Levitt, M. H. *J. Magn. Res.*, **1998**, *132*, 220-239.
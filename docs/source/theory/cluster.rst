.. _cluster:

Ray Clustering
==============

In order to cluster together unblocked rays into meaningful groups ``atom_access`` uses the single-linkage
agglomerative clustering algorithm implemented in `Scikit-learn <https://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering>`_.
This successively merges individual rays to form clusters by considering the Cartesian distance between the tip of each ray.
Rays are merged when this distance falls below a given threshold,
which we define as the maximum 8th nearest neighbor distance (Figure 1),
for a set of unit rays defined by the ZCW algorithm. This value
can be computed on-the-fly, though we have hard-coded each ZCW index up to :math:`rho = 16`.

Unblocked rays are clustered such that each ray's adjacent and diagonally adjacent
neighbours are included in the same cluster, effectively tracing out individual binding sites.

The size of each cluster is calculated a percentage of the total number of rays. As the ZCW rays evenly
sample points on the surface of a sphere, the size of the cluster is equivalent
to the percentage solid angle subtended by the cluster (for sufficiently dense
ZCW angular grids).

.. figure:: cluster_distance.png
    :align: center

    Figure 1: ZCW rays on a unit sphere illustrating the eighth nearest neighbor distance (orange), and distances to the seven closest neighbors (black) for a particular ray

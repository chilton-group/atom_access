.. _trace:

Ray Tracing
===========

With the spheres (atoms) and rays :ref:`defined <rays>`, ``atom_access`` then performs a ray tracing procedure to find accessible sites at
the specified atom of interest.

The Ray-Sphere Interaction Method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The intersection of a ray with a sphere can be found using the Ray-Sphere interaction method (Figure 1), which consists of the
following steps.

Consider the interaction of a single unit-ray :math:`\hat{\bf{u}}` which emanates from the origin :math:`\text{O}` and may or may not intersect a
sphere of radius :math:`r`, with a centre specified by the position vector :math:`\vec{\bf{C}}` (Figure 1). Assuming that the origin is
not enclosed within this sphere, we seek to find the intersection point(s) :math:`p_1` and :math:`p_2` where the ray enters and
leaves the sphere, if they exist at all.

The dot product :math:`p = \hat{\bf{u}} \cdot \vec{\bf{C}}` defines the projection of :math:`\vec{\bf{C}}` onto the ray (Figure S1).
If :math:`p` is negative, then :math:`\vec{\bf{C}}` and :math:`\hat{\bf{u}}` are in opposite directions and we immediately conclude that there is no intersection.
For rays with positive :math:`p`, we define the quantity :math:`d` as the shortest distance between the centre of the sphere and the ray.

Using Pythagoras' Theorem 

.. math ::

    d^2 = |\vec{\bf{C}}|^2 - p^2

we then have three possibilities

1. :math:`d^2 > r^2` : The ray does not intersect with the sphere
2. :math:`d^2 = r^2` : The ray intersects with the sphere once
3. :math:`d^2 < r^2` : The ray intersects with the sphere twice

In the final two cases, the intersections occur at a distance :math:`p \pm t` along the ray, where :math:`t^2=r^2-d^2`, and so the
intersection points :math:`p_1` and :math:`p_2` are defined by the vector equations

.. math ::

    p_1 &= O + (p-t)\hat{\bf{u}}\\
    p_2 &= O + (p+t)\hat{\bf{u}}

where, if the ray enters and leaves the sphere at the same point, then :math:`t=0` and :math:`p_1=p_2`.

``Atom_access`` calculates both :math:`p_1` and :math:`p_2`, but only the former is retained as this point is closest to the origin.

.. figure:: ray_tracing.png
    :align: center

    Figure 1: Line-Sphere Interaction Method

Processing the Rays
^^^^^^^^^^^^^^^^^^^

``atom_access`` carries out the above process, calculating every possible interaction of a given ray with all of the atoms
of a the molecule. Those rays which have no intersections are grouped together as `unblocked` rays, while those which intersect
with at least one atom are considered to be `blocked`. The total percentage of unblocked rays is calculated as a fraction of
the total number of rays.
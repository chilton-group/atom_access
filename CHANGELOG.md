# CHANGELOG


## v2.3.2 (2025-02-26)

### Documentation

- Update readme [skip-ci]
  ([`331bb89`](https://gitlab.com/chilton-group/atom_access/-/commit/331bb89ff637613e50a0ab4e5e67557afdfdd00a))

- Update web app link [skip-ci]
  ([`34b1042`](https://gitlab.com/chilton-group/atom_access/-/commit/34b1042deaad434310f534de620db40b6132a2ab))


## v2.3.1 (2023-10-09)

### Documentation

- Add image [skip-ci]
  ([`04b83ef`](https://gitlab.com/chilton-group/atom_access/-/commit/04b83eff2efd294b03dc16418568ddc9811c40c5))

- Fix footnotes
  ([`5a05a03`](https://gitlab.com/chilton-group/atom_access/-/commit/5a05a03f8ee09097fe7499c98c89ef77a0ae4c16))

- Fix typo
  ([`adeb06e`](https://gitlab.com/chilton-group/atom_access/-/commit/adeb06e9c3a8b109eae9da515eeb3f43b52ea2c0))

- Fix typo
  ([`18c2dfe`](https://gitlab.com/chilton-group/atom_access/-/commit/18c2dfe5e266daa5a3d3feb1283562d84722a415))

- Improve logo load time, add svg logo [skip-ci]
  ([`ddb4e42`](https://gitlab.com/chilton-group/atom_access/-/commit/ddb4e42f1f82a69d6978d18064d0587c535e5f8e))

- Update docstring [skip-ci]
  ([`c69cf13`](https://gitlab.com/chilton-group/atom_access/-/commit/c69cf13bb02ef4dd59b5f0f2f7a9774f8ea7d156))

- Update documentation to match manuscript
  ([`c6be35e`](https://gitlab.com/chilton-group/atom_access/-/commit/c6be35e136632051eed0a42c7204f2209b8e4bb3))

- Update paper refs
  ([`e788366`](https://gitlab.com/chilton-group/atom_access/-/commit/e7883660d35a664d6a3ea56e5974827b0b005d94))

- Update ray tracing diagram
  ([`34d29f8`](https://gitlab.com/chilton-group/atom_access/-/commit/34d29f817af75153bb7b342aa5915ca0dd9b02e9))

update ray tracing diagram to use lowercase letters

- Update reference
  ([`cd5f9d0`](https://gitlab.com/chilton-group/atom_access/-/commit/cd5f9d003eeb03976a75c2fb1507bd835545ce0b))


## v2.3.0 (2023-08-11)

### Build System

- Update pyproject.toml for new version of python-semantic-release [ci-skip]
  ([`1c7f41a`](https://gitlab.com/chilton-group/atom_access/-/commit/1c7f41a2a78e33ff47523bc36134a1ae422917d4))

- Update xyz_py dependency
  ([`fc36242`](https://gitlab.com/chilton-group/atom_access/-/commit/fc3624270392ac97f6c25e5488f552216ddfa0c6))

### Continuous Integration

- Update pipeline for new python-semantic-release version [ci skip]
  ([`5ac7382`](https://gitlab.com/chilton-group/atom_access/-/commit/5ac73820ac3b64b799de8bc2d2111b40543c45f0))

### Documentation

- Update docs version variable name [ci-skip]
  ([`cf3a70c`](https://gitlab.com/chilton-group/atom_access/-/commit/cf3a70cb597728a0e2fbbe2f448148d06b65fb8d))

### Refactoring

- Add ray classmethod, improve printing
  ([`5cbe91b`](https://gitlab.com/chilton-group/atom_access/-/commit/5cbe91bc286142ee17884b205d67009417f165b1))


## v2.2.0 (2023-06-01)

### Documentation

- Add docstrings and typehints
  ([`00eb29b`](https://gitlab.com/chilton-group/atom_access/-/commit/00eb29bacf4363ff76c9b030f5fcc7c15f1ff3a1))

- Improve documentation and remove reference to specific functions in code
  ([`09546ab`](https://gitlab.com/chilton-group/atom_access/-/commit/09546abd850b05bdb915a946ce22645b421d9249))
